import { createRouter, createWebHistory } from "vue-router";
import HomePage from "../pages/HomePage.vue";
import ActivePage from "../pages/ActivePage.vue";
import LoginPage from "../pages/LoginPage.vue";
import AccountPage from "../pages/AccountPage.vue";
import ReferralPage from "../pages/ReferralPage.vue"
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: "/", name: "HomePage", component: HomePage },
    { path: "/active", name: "ActivePage", component: ActivePage },
    {
      path: "/login",
      name: "LoginPage",
      component: LoginPage,
    },
    {
      path: "/account",
      name: "AccountPage",
      component: AccountPage
    },
    {
      path: "/referral-link",
      name: "ReferralPage",
      component: ReferralPage
    }
  ],
});

export default router;
